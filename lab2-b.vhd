
-- This file has been automatically generated by SimUAid.

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

library SimUAid_synthesis;
use SimUAid_synthesis.SimuAid_synthesis_pack.all;

entity ent1 is
port(A, B, C, D, E: in STD_LOGIC;

	W, X, Y, Z: out STD_LOGIC
	);
end ent1;

architecture Structure of ent1 is
	signal Vnet_0, Bn, Dn, En, X1, X2, X3, X4, X6, X5, 
		X7, X8, X9, X10, Yp: STD_LOGIC;
begin
	VHDL_Device_0: inverter port map (A, Vnet_0);
	VHDL_Device_1: inverter port map (B, Bn);
	VHDL_Device_2: inverter port map (D, Dn);
	VHDL_Device_3: inverter port map (E, En);
	VHDL_Device_4: nor2 port map (B, C, X1);
	VHDL_Device_5: nor2 port map (C, D, X2);
	VHDL_Device_6: nor2 port map (C, E, X3);
	VHDL_Device_7: nor2 port map (B, D, X4);
	VHDL_Device_8: nor2 port map (A, C, X6);
	VHDL_Device_9: nor2 port map (Bn, En, Z);
	VHDL_Device_10: nor3 port map (Bn, C, En, X5);
	VHDL_Device_11: nor4 port map (Vnet_0, X1, X2, X3, W);
	VHDL_Device_12: nor4 port map (X4, X2, X5, X6, X);
	VHDL_Device_13: nor3 port map (A, Dn, Bn, X7);
	VHDL_Device_14: nor3 port map (Vnet_0, En, B, X8);
	VHDL_Device_15: nor3 port map (Vnet_0, D, En, X9);
	VHDL_Device_16: nor3 port map (Bn, Dn, E, X10);
	VHDL_Device_17: nor4 port map (X7, X8, X9, X10, Yp);
	VHDL_Device_18: inverter port map (Yp, Y);
end Structure;