pkg load statistics;

% Step 1
cases = 200;

mu_1 = [60, 80];
cov_1 = [300, 30; 20, 200]; 
S_1 = mvnrnd(mu_1, cov_1, cases);

mu_2 = [-10, 40];
cov_2 = [90, 100; 50, 300]; 
S_2 = mvnrnd(mu_2, cov_2, cases);

mu_3 = [20, 15];
cov_3 = [950, 70; 20, 200]; 
S_3 = mvnrnd(mu_3, cov_3, cases);

% Step 2
X_1 = reshape(S_1(:,1),2,[]);
X_1(3,:) = nan;
X_1 = X_1(:);
Y_1 = reshape(S_1(:,2),2,[]);
Y_1(3,:) = nan;
Y_1 = Y_1(:);

X_2 = reshape(S_2(:,1),2,[]);
X_2(3,:) = nan;
X_2 = X_2(:);
Y_2 = reshape(S_2(:,2),2,[]);
Y_2(3,:) = nan;
Y_2 = Y_2(:);

X_3 = reshape(S_3(:,1),2,[]);
X_3(3,:) = nan;
X_3 = X_3(:);
Y_3 = reshape(S_3(:,2),2,[]);
Y_3(3,:) = nan;
Y_3 = Y_3(:);

S_mu_1 = mean(S_1);
S_cov_1 = cov(S_1);
S_mu_2 = mean(S_2);
S_cov_2 = cov(S_2);
S_mu_3 = mean(S_3);
S_cov_3 = cov(S_3);

D_1 = [50, 50];
D_2 = [10, 20];
D_3 = [0, 20];
plot(X_1, Y_1, 'ro', X_2, Y_2, 'bo', X_3, Y_3, 'go',
     50, 50, 'm*', 10, 20, 'm+', 0, 20, 'mx');

P_11 = ((2*pi)*det(S_cov_1)^0.5)^-1 * exp(-0.5 * (D_1-S_mu_1) * (S_cov_1^-1) * (D_1-S_mu_1)')
P_12 = ((2*pi)*det(S_cov_2)^0.5)^-1 * exp(-0.5 * (D_1-S_mu_2) * (S_cov_2^-1) * (D_1-S_mu_2)')
P_13 = ((2*pi)*det(S_cov_3)^0.5)^-1 * exp(-0.5 * (D_1-S_mu_3) * (S_cov_3^-1) * (D_1-S_mu_3)')

P_21 = ((2*pi)*det(S_cov_1)^0.5)^-1 * exp(-0.5 * (D_2-S_mu_1) * (S_cov_1^-1) * (D_1-S_mu_1)')
P_22 = ((2*pi)*det(S_cov_2)^0.5)^-1 * exp(-0.5 * (D_2-S_mu_2) * (S_cov_2^-1) * (D_1-S_mu_2)')
P_23 = ((2*pi)*det(S_cov_3)^0.5)^-1 * exp(-0.5 * (D_2-S_mu_3) * (S_cov_3^-1) * (D_1-S_mu_3)')

P_31 = ((2*pi)*det(S_cov_1)^0.5)^-1 * exp(-0.5 * (D_3-S_mu_1) * (S_cov_1^-1) * (D_1-S_mu_1)')
P_32 = ((2*pi)*det(S_cov_2)^0.5)^-1 * exp(-0.5 * (D_3-S_mu_2) * (S_cov_2^-1) * (D_1-S_mu_2)')
P_33 = ((2*pi)*det(S_cov_3)^0.5)^-1 * exp(-0.5 * (D_3-S_mu_3) * (S_cov_3^-1) * (D_1-S_mu_3)')

% Sample output
% P_11 =    7.1446e-05
% P_12 =    2.1911e-16
% P_13 =    1.3821e-05
% D₁ is probably in class 1, since P(x = D₁|ω₁) is the greatest.
%
% P_21 =    4.4062e-06
% P_22 =    1.7001e-09
% P_23 =    3.1017e-04
% D₂ is probably in class 3, since P(x = D₂|ω₃) is the greatest.
%
% P_31 =    3.7329e-06
% P_32 =    3.0894e-07
% P_33 =    3.3487e-04
% D₃ is probably in class 3, since P(x = D₃|ω₃) is the greatest.
%
% We only need to compare these probabilities since we assumed that the a priori probabilities are equal.
